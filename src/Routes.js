import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from './screen/HomePage/HomeScreen';
import DetailKMP from './screen/HomePage/DetailKMP';
import MasterKamus from './screen/AdminPage/MasterKamus';

const Stack = createStackNavigator();
import {View, Text} from 'react-native';

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="HomeScreen"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="HomeScreen" component={HomeScreen} />
        <Stack.Screen name="DetailKMP" component={DetailKMP} />
        <Stack.Screen name="MasterKamus" component={MasterKamus} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
