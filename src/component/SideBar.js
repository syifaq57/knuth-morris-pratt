/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Image,
  AsyncStorage,
  BackHandler,
  Alert,
  TouchableOpacity,
} from 'react-native';
import {Text} from 'native-base';
import colors from '../res/colors/index';
import Modal from 'react-native-modal';
// import Rate, {AndroidMarket} from 'react-native-rate';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

import {useNavigation, useFocusEffect} from '@react-navigation/native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import LoginMasterData from './LoginMasterData';

const listMenu = [
  {
    id: 'dashboard',
    title: 'Dashboard',
    icon: 'home',
  },
  {
    id: 'master',
    title: 'Master Data Kamus',
    icon: 'archive',
  },
  {
    id: 'exit',
    title: 'Exit',
    icon: 'power-off',
  },
];

const SideBar = props => {
  const navigation = useNavigation();
  const [state, setState] = useState({
    user: null,
    listMenu: listMenu,
    loginVisible: false,
  });

  const updateState = newData => {
    setState(prev => ({...prev, ...newData}));
  };

  const setVisibleLogin = flag => {
    updateState({
      loginVisible: flag,
    });
  };

  const onLogout = async () => {
    BackHandler.exitApp();
  };

  const logoutAlert = () => {
    Alert.alert('Logout!', 'Apakah Kamu yakin ingin Logout ?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => onLogout()},
    ]);
  };

  const onSelectMenu = menu => {
    switch (menu) {
      case 'dashboard':
        navigation.navigate('HomeScreen');
        break;
      case 'master':
        // navigation.navigate('MasterKamus');
        setVisibleLogin(true);
        break;
      case 'exit':
        logoutAlert();
        break;
      default:
        break;
    }
  };

  // const preparedScreen = async () => {
  //   await AsyncStorage.getItem('userData').then(item => {
  //     const user = JSON.parse(item);
  //     if (user && Object.keys(user).length > 0) {
  //       updateState({
  //         user: user,
  //       });
  //       // console.log('user', user)
  //       // if (user.role !== 'Admin') {
  //       //   updateState({listMenu: listMenuUser});
  //       // }
  //     }
  //   });
  // };

  const getListMenu = () => {
    return listMenu;
  };

  useEffect(() => {
    // preparedScreen();
  }, []);

  return (
    <View>
      <Modal
        isVisible={props.isVisible}
        style={{marginVertical: 0, marginHorizontal: 0}}
        backdropOpacity={0.3}
        animationIn={'slideInLeft'}
        animationOut={'slideOutLeft'}
        onBackdropPress={props.onBackButtonPress}
        onBackButtonPress={props.onBackButtonPress}>
        <View
          style={{
            backgroundColor: 'white',
            height: '100%',
            width: '65%',
            // alignItems: 'center',
            // paddingTop: hp(10),
          }}>
          <View
            style={{
              flex: 1.3,
              alignItems: 'center',
              backgroundColor: colors.primarydark,
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: wp(5),
                textAlign: 'center',
                color: 'white',
                fontWeight: 'bold',
              }}>
              Kamus Kebidanan
            </Text>
          </View>
          <View style={{flex: 6, paddingTop: hp(2), marginHorizontal: wp(4)}}>
            {getListMenu().map((data, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  props.onBackButtonPress();
                  onSelectMenu(data.id);
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginVertical: hp(2),
                  }}>
                  <View
                    style={{
                      flex: 0.5,
                      alignItems: 'center',
                      marginRight: wp(2.5),
                      marginTop: hp(0.2),
                    }}>
                    <FontAweSome
                      name={data.icon}
                      color={colors.black}
                      size={hp(3)}
                    />
                  </View>

                  <Text
                    style={{
                      flex: 5,
                      fontSize: wp(4),
                      fontWeight: 'bold',
                      color: colors.grayPrimary,
                    }}>
                    {data.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </Modal>
      <LoginMasterData
        isVisible={state.loginVisible}
        onBackButtonPress={() => setVisibleLogin(false)}
        navigation={navigation}
      />
    </View>
  );
};

export default SideBar;
