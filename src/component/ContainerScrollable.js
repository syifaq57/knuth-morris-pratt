import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  ScrollView,
} from 'react-native';
import colors from '../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ContainerScrollable = props => {
  return (
    <ImageBackground
      style={styles.container}
      source={require('../res/images/bg.jpg')}>
      <ScrollView style={styles.scroll} keyboardShouldPersistTaps="handled">
        {props.children}
      </ScrollView>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  scroll: {
    paddingHorizontal: wp(4),
  },
});

export default ContainerScrollable;
