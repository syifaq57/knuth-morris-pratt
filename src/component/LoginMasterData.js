/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import colors from '../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ReTextInput from './ReTextInput';

import database from '@react-native-firebase/database';

const initialState = {
  username: '',
  password: '',
};

const LoginMasterData = props => {
  const [state, setState] = useState(initialState);

  const updateState = newData => {
    setState(prev => ({...prev, ...newData}));
  };

  const _dismiss = () => {
    props.onBackButtonPress();
    setState(initialState);
  };

  const onLogin = () => {
    const data = {
      username: state.username,
      password: state.password,
    };
    database()
      .ref('admin')
      .once('value')
      .then(snapshot => {
        //
        if (
          snapshot.val().username === data.username &&
          snapshot.val().password === data.password
        ) {
          _dismiss();

          props.navigation.navigate('MasterKamus');
        } else {
          alert('Username / Password tidak sesuai')
        }
      });
  };

  return (
    <Modal
      isVisible={props.isVisible}
      style={{marginVertical: 0, marginHorizontal: wp(7)}}
      backdropOpacity={0.3}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      onBackdropPress={() => _dismiss()}
      onBackButtonPress={() => _dismiss()}>
      <View
        style={{
          backgroundColor: colors.white,
          borderRadius: wp(1.5),
          height: hp(45),
        }}>
        <View
          style={{
            height: hp(6),
            backgroundColor: colors.blue01,
            borderTopLeftRadius: wp(1.5),
            borderTopRightRadius: wp(1.5),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{fontSize: wp(4), fontWeight: 'bold', color: colors.white}}>
            Login Master Data
          </Text>
        </View>
        <View
          style={{flex: 1, paddingVertical: hp(2), paddingHorizontal: wp(5)}}>
          <View style={{marginBottom: hp(4), marginTop: hp(2)}}>
            <ReTextInput
              value={state.username}
              onChangeText={val => {
                updateState({username: val});
              }}
              label="Username"
            />
          </View>
          <View style={{marginBottom: hp(2)}}>
            <ReTextInput
              value={state.password}
              onChangeText={val => {
                updateState({password: val});
              }}
              label="Password"
              passwordInput
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: wp(5),
            paddingVertical: hp(2),
            marginBottom: hp(0.5),
          }}>
          <TouchableOpacity
            onPress={() => _dismiss()}
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: colors.warning,
              marginRight: wp(5),
              height: hp(5),
              borderRadius: wp(1),
              justifyContent: 'center',
            }}>
            <Text
              style={{
                marginTop: hp(-0.5),
                fontSize: wp(3.2),
                fontWeight: 'bold',
                color: colors.white,
              }}>
              CANCEL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onLogin();
            }}
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: colors.blue01,
              marginLeft: wp(5),
              borderRadius: wp(1),
              justifyContent: 'center',
              height: hp(5),
            }}>
            <Text
              style={{
                marginTop: hp(-0.5),
                fontSize: wp(3.2),
                fontWeight: 'bold',
                color: colors.white,
              }}>
              LOGIN
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default LoginMasterData;
