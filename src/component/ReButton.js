/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, TextInput, TouchableOpacity} from 'react-native';
import {Text} from 'native-base';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReButton = props => {
  return (
    <View>
      <TouchableOpacity
        style={{
          paddingVertical: hp(1.7),
          backgroundColor: props.backgroundColor
            ? props.backgroundColor
            : props.disabled
            ? colors.gray06
            : colors.primarydark,
          alignItems: 'center',
          borderRadius: wp(1),
          ...props.style,
        }}
        disabled={props.disabled ? props.disabled : false}
        onPress={props.onPress}>
        <Text
          style={{
            fontSize: wp(4.2),
            fontWeight: 'bold',
            color: props.disabled ? colors.primarydark : 'white',
          }}>
          {props.label.toUpperCase()}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ReButton;
