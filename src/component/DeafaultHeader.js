/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {Text, Header} from 'native-base';
import colors from '../res/colors/index';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

import SideBar from '../component/SideBar';

const DeafaultHeader = props => {
  const [visibleSideBar, setVisibleSideBar] = useState(false);
  const navigation = useNavigation();

  return (
    <Header
      transparent
      style={{
        backgroundColor: colors.blue01,
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          flexDirection: 'row',
          marginHorizontal: wp(0.5),
          alignContent: 'center',
        }}>
        {props.backButton ? (
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: hp(0.4),
              marginLeft: wp(1),
            }}
            onPress={() => {
              navigation.goBack();
            }}>
            <Ionicons
              name="arrow-back-sharp"
              color={colors.white}
              size={hp(4.5)}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: hp(0.4),
            }}
            onPress={() => {
              setVisibleSideBar(true);
            }}>
            <Ionicons name="md-menu" color={colors.white} size={hp(4.5)} />
          </TouchableOpacity>
        )}
        <View style={{marginLeft: wp(3), textAlign: 'center'}}>
          <Text
            style={{
              fontSize: hp(3),
              color: colors.white,
              fontWeight: 'bold',
            }}>
            {props.title}
          </Text>
        </View>
      </View>
      <SideBar
        name={'state.nama'}
        isVisible={visibleSideBar}
        onBackButtonPress={() => setVisibleSideBar(false)}
      />
    </Header>
  );
};

export default DeafaultHeader;
