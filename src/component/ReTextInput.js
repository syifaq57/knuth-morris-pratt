/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Animated,
  StyleSheet,
} from 'react-native';
import FontAweSome from 'react-native-vector-icons/FontAwesome';
import colors from '../res/colors/index';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const ReTextInput = props => {
  return (
    <View>
      <Text
        style={{
          fontSize: wp(2.9),
          fontWeight: 'bold',
          marginLeft: wp(0.5),
          marginBottom: hp(0.1),
        }}>
        {props.label}
      </Text>
      <View
        style={{
          borderWidth: 1.5,
          borderRadius: wp(1),
          justifyContent: 'center',
          borderColor: colors.gray05,
        }}>
        <TextInput
          value={props.value}
          multiline={props.multiLine || props.textArea}
          keyboardType={props.numeric ? 'numeric' : 'default'}
          editable={props.editable}
          onChangeText={text => props.onChangeText(text)}
          placeholderTextColor={colors.gray11}
          style={{
            fontSize: wp(3.5),
            fontFamily: 'Poppins-Regular',
            color: colors.black,
            height: props.textArea ? hp(20) : null,
            textAlignVertical: props.textArea ? 'top' : 'center',
          }}
          placeholder={
            props.placeholder
              ? props.placeholder
              : `Input ${props.label.toLowerCase()}`
          }
          secureTextEntry={props.passwordInput ? true : false}
        />
      </View>
    </View>
  );
};

export default ReTextInput;
