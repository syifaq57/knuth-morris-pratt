import React, {useRef} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import DeafaultHeader from '../../component/DeafaultHeader';
import colors from '../../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import ContainerScrollable from '../../component/ContainerScrollable';
import KamusList from './KamusList';
import AddKamusModal from './AddKamusModal';

const MasterKamus = () => {
  const addKamusModal = useRef(null);

  const openKamusModal = val => {
    addKamusModal.current?.openModal(val);
  };

  return (
    <View style={styles.container}>
      <DeafaultHeader title="Master Kamus" menuBar />
      <ContainerScrollable>
        <KamusList
          onAddKamus={val => {
            openKamusModal(val);
          }}
        />
        <AddKamusModal ref={addKamusModal} />
      </ContainerScrollable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  body: {
    flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
  },
});

export default MasterKamus;
