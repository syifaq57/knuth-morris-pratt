/* eslint-disable react-native/no-inline-styles */
import React, {forwardRef, useState, useImperativeHandle} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import colors from '../../res/colors';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import FontAweSome from 'react-native-vector-icons/FontAwesome';
import ReTextInput from '../../component/ReTextInput';

import database from '@react-native-firebase/database';

const initialState = {
  id: '',
  title: '',
  desc: '',
  loadingSave: false,
};

const AddKamusModal = ({}, ref) => {
  const [visible, setVisible] = useState(false);
  const [state, setState] = useState(initialState);

  const updateState = newData => {
    setState(prev => ({...prev, ...newData}));
  };

  const _dismiss = () => {
    setVisible(false);
    updateState({
      id: '',
      title: '',
      desc: '',
    });
  };

  const openModal = data => {
    if (!data) {
      setVisible(true);
    } else {
      setVisible(true);
      updateState({...data});
    }
  };

  const setLoadingSave = val => {
    updateState({
      loadingSave: val,
    });
  };

  useImperativeHandle(ref, () => ({
    openModal,
  }));

  const onSaveKamus = () => {
    setLoadingSave(true);
    const data = {
      title: state.title,
      desc: state.desc,
    };

    let id = '';

    if (state.id?.length > 0) {
      id = state.id;
    } else {
      id = `KMP-${Date.now().toString()}`;
    }

    database()
      .ref(`/kamus/${id}`)
      .set(data)
      .then(res => {
        console.log('Data set.', res);
        setLoadingSave(false);
        _dismiss();
      })
      .catch(e => {
        console.log('e', e);
        setLoadingSave(false);
      });
  };

  return (
    <Modal
      isVisible={visible}
      backdropOpacity={0.3}
      animationIn={'fadeIn'}
      animationOut={'fadeOut'}
      onBackdropPress={() => {
        _dismiss();
      }}
      onBackButtonPress={() => {
        _dismiss();
      }}>
      <View
        style={{
          backgroundColor: colors.white,
          borderRadius: wp(1.5),
          height: hp(55),
        }}>
        <View
          style={{
            height: hp(6),
            backgroundColor: colors.blue01,
            borderTopLeftRadius: wp(1.5),
            borderTopRightRadius: wp(1.5),
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{fontSize: wp(4), fontWeight: 'bold', color: colors.white}}>
            TAMBAH KAMUS
          </Text>
        </View>
        <View
          style={{flex: 1, paddingVertical: hp(2), paddingHorizontal: wp(2.3)}}>
          <View style={{marginBottom: hp(2)}}>
            <ReTextInput
              value={state.title}
              onChangeText={val => {
                updateState({title: val});
              }}
              label="Title"
            />
          </View>
          <View>
            <ReTextInput
              value={state.desc}
              onChangeText={val => {
                updateState({desc: val});
              }}
              label="Deskripsi"
              textArea
            />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: wp(4),
            paddingVertical: hp(2),
            marginBottom: hp(0.5),
          }}>
          <TouchableOpacity
            onPress={() => _dismiss()}
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: colors.warning,
              marginRight: wp(5),
              height: hp(4.5),
              borderRadius: wp(1),
              justifyContent: 'center',
            }}>
            <Text
              style={{
                marginTop: hp(-0.5),
                fontSize: wp(3.2),
                fontWeight: 'bold',
                color: colors.white,
              }}>
              CANCEL
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              onSaveKamus();
            }}
            style={{
              flex: 1,
              alignItems: 'center',
              backgroundColor: colors.greenDefault,
              marginLeft: wp(5),
              borderRadius: wp(1),
              justifyContent: 'center',
            }}>
            <Text
              style={{
                marginTop: hp(-0.5),
                fontSize: wp(3.2),
                fontWeight: 'bold',
                color: colors.white,
              }}>
              SAVE
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default forwardRef(AddKamusModal);
