/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {Card} from 'native-base';
import colors from '../../res/colors';

import LinearGradient from 'react-native-linear-gradient';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import database from '@react-native-firebase/database';

const KamusList = props => {
  const [kamusList, setKamusList] = useState([]);

  const fetchData = async () => {
    database()
      .ref('kamus')
      .orderByChild('title')
      .on('value', snapshot => {
        const kamuslist = [];
        snapshot.forEach(item => {
          kamuslist.push({
            id: item.key,
            title: item.val().title,
            desc: item.val().desc,
          });
        });
        setKamusList(kamuslist);
      });
  };

  const deletKamus = id => {
    database().ref(`/kamus/${id}`).remove();
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <View style={{paddingVertical: hp(1)}}>
      <TouchableOpacity onPress={() => props.onAddKamus()}>
        <Card
          style={{
            marginVertical: hp(2),
            borderRadius: wp(2),
            backgroundColor: colors.primary,
          }}>
          <View
            style={{
              flexDirection: 'row',
              borderRadius: wp(2),
              height: hp(7),
              alignItems: 'center',
              paddingHorizontal: wp(3),
              backgroundColor: colors.greenDefault,
            }}>
            <View style={{flex: 6, marginLeft: wp(2)}}>
              <Text
                ellipsizeMode="tail"
                numberOfLines={1}
                style={{
                  color: colors.white,
                  fontSize: wp(4.5),
                  fontWeight: 'bold',
                  marginBottom: hp(0.5),
                }}>
                Tambah Daftar Kamus
              </Text>
            </View>
            <View style={{flex: 0.5}}>
              <TouchableOpacity onPress={() => {}}>
                <FontAweSome name="plus" color={colors.white} size={hp(3.2)} />
              </TouchableOpacity>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
      {kamusList.map((data, index) => (
        <TouchableOpacity key={index} onPress={() => props.onAddKamus(data)}>
          <Card
            style={{
              marginVertical: hp(2),
              borderRadius: wp(2),
              backgroundColor: colors.primary,
            }}>
            <LinearGradient
              start={{x: 0.0, y: 0.25}}
              end={{x: 0.5, y: 1.0}}
              colors={[colors.blue01, colors.bgErm]}
              style={{
                flexDirection: 'row',
                borderRadius: wp(2),
                height: hp(7),
                alignItems: 'center',
                paddingHorizontal: wp(3),
              }}>
              <View style={{flex: 0.7, marginRight: wp(1)}}>
                <Image
                  source={require('../../res/images/book.png')}
                  style={{
                    height: hp(4),
                    width: hp(4),
                    resizeMode: 'contain',
                  }}
                />
              </View>
              <View style={{flex: 6}}>
                <Text
                  ellipsizeMode="tail"
                  numberOfLines={1}
                  style={{
                    color: colors.primarydark,
                    fontSize: wp(3.2),
                    fontFamily: 'JosefinSans-Bold',
                    marginBottom: hp(0.5),
                  }}>
                  {data?.title || ''}
                </Text>
              </View>
              <View style={{flex: 0.5}}>
                <TouchableOpacity
                  onPress={() => {
                    deletKamus(data.id);
                  }}>
                  <FontAweSome
                    name="trash"
                    color={colors.primarydark}
                    size={hp(3.2)}
                  />
                </TouchableOpacity>
              </View>
            </LinearGradient>
          </Card>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default KamusList;
