/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import colors from '../../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import DeafaultHeader from '../../component/DeafaultHeader';
import ContainerScrollable from '../../component/ContainerScrollable';

import {useRoute} from '@react-navigation/native';
import {Card} from 'native-base';

import FontAweSome from 'react-native-vector-icons/FontAwesome';

const initialState = {
  pattern: '',
  word: '',
  kmpList: [],
  kmpWord: [],

  isLoading: false,
};

const DetailKMP = () => {
  const route = useRoute();

  const [state, setState] = useState(initialState);

  const updateState = newData => {
    setState(prev => ({
      ...prev,
      ...newData,
    }));
  };

  const setLoading = flag => {
    updateState({
      isLoading: flag,
    });
  };

  const initialScreen = () => {
    setLoading(true);

    let detail = route?.params?.detail;

    let teksLength = 0;
    let teks = '';
    let indexWord = 0;
    let word = '';

    const output = [];

    if (detail) {
      detail = JSON.parse(detail);
      teksLength = detail.detailText.title.length;
      teks = detail.detailText.title;
      indexWord = detail.indexFound.indexWord;
      word = detail.searchText;
    }

    for (let z = 0; z <= indexWord; z++) {
      let charPushedLength = 0;
      let blankList = [];
      for (let j = 0; j < teksLength; j++) {
        if (blankList.length == teksLength) {
          break;
        }

        if (j < z || word.length === charPushedLength) {
          blankList.push('');
        } else {
          for (let k = 0; k < word.length; k++) {
            blankList.push(word[k]);
            charPushedLength++;
          }
        }
      }
      // console.log('words', blankList);
      output.push(blankList);
    }

    const strings = [];
    for (let i = 0; i < teksLength; i++) {
      strings.push(teks[i]);
    }
    console.log(strings);
    console.log(output);
    updateState({
      pattern: detail.searchText,
      word: teks,
      kmpList: output,
      kmpWord: strings,
    });

    setLoading(false);
  };

  useEffect(() => {
    initialScreen();
  }, []);

  return (
    <View style={styles.container}>
      <DeafaultHeader title="Knuth Morris Pratt" backButton />
      {state.isLoading ? (
        <View style={{paddingVertical: hp(3)}}>
          <ActivityIndicator size="large" color={colors.blue01} />
        </View>
      ) : (
        <ContainerScrollable>
          <View style={{maxHeight: wp(80)}}>
            <Card
              style={{
                marginTop: hp(2),
                paddingHorizontal: wp(2.5),
                paddingVertical: hp(1),
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: wp(2),
                backgroundColor: colors.blue01,
              }}>
              <Text
                style={{
                  fontSize: wp(3),
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                  color: 'black',
                }}>
                Pattern
              </Text>
              <Text
                style={{
                  fontSize: wp(3.8),
                  marginBottom: hp(1),
                  color: 'white',
                }}>
                {state.pattern.toUpperCase()}
              </Text>
              <Text
                style={{
                  fontSize: wp(3),
                  fontWeight: 'bold',
                  borderBottomWidth: 1,
                }}>
                Match String
              </Text>
              <Text style={{fontSize: wp(3.8), color: 'white'}}>
                {state.word.toUpperCase()}
              </Text>
            </Card>
          </View>

          <Card
            style={{paddingHorizontal: wp(2), backgroundColor: colors.gray12}}>
            <ScrollView horizontal style={{marginVertical: hp(2)}}>
              <View>
                {state.kmpList?.map((pat, index) => (
                  <View style={{marginVertical: hp(1)}}>
                    <View style={{flexDirection: 'row'}}>
                      <FontAweSome
                        name={
                          index !== state.kmpList.length - 1
                            ? 'remove'
                            : 'check'
                        }
                        color={
                          index !== state.kmpList.length - 1
                            ? colors.red
                            : colors.greenDefault
                        }
                        size={hp(2.5)}
                      />

                      <Text
                        style={{
                          marginLeft: wp(2),
                          fontSize: wp(3),
                          fontWeight: 'bold',
                        }}>{`Step-${index}`}</Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        borderWidth: 2,
                        borderColor:
                          index !== state.kmpList.length - 1
                            ? colors.blue01
                            : colors.greenDefault,
                        borderBottomWidth: 1,
                      }}>
                      {pat?.map((patword, i) => (
                        <View
                          key={i}
                          style={{
                            backgroundColor:
                              i % 2 ? colors.gray12 : colors.white,
                            width: wp(7),
                            height: wp(7),
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRightWidth:
                              i === state.kmpWord.length - 1 ? 0 : 1,
                            borderColor: colors.blue01,
                          }}>
                          <Text>{patword.toUpperCase()}</Text>
                        </View>
                      ))}
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        borderWidth: 2,
                        borderTopWidth: 1,
                        borderColor:
                          index !== state.kmpList.length - 1
                            ? colors.blue01
                            : colors.greenDefault,
                      }}>
                      {state.kmpWord?.map((word, i) => (
                        <View
                          key={i.toString()}
                          style={{
                            backgroundColor:
                              i % 2 ? colors.white : colors.gray12,
                            width: wp(7),
                            height: wp(7),
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRightWidth:
                              i === state.kmpWord.length - 1 ? 0 : 1,
                            borderColor: colors.blue01,
                          }}>
                          <Text>{word.toUpperCase()}</Text>
                        </View>
                      ))}
                    </View>
                  </View>
                ))}
              </View>
            </ScrollView>
          </Card>
        </ContainerScrollable>
      )}
    </View>
  );
};

export default DetailKMP;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  body: {
    flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
  },
});
