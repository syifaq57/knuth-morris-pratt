/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import {Card} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import ReButton from '../../component/ReButton';
import FontAweSome from 'react-native-vector-icons/FontAwesome';

const initialState = {
  searchText: '',
};

const SearchMenu = props => {
  const [state, setState] = useState(initialState);

  const updateState = newData => {
    setState(prev => ({
      ...prev,
      ...newData,
    }));
  };
  return (
    <View style={{flex: 2}}>
      <Card
        style={{
          height: hp(10),
          backgroundColor: colors.white,
          borderRadius: wp(0.5),
          paddingHorizontal: wp(1.5),
        }}>
        <TextInput
          // ref={inputref}
          multiline
          value={state.searchText}
          keyboardType={props.numeric ? 'numeric' : 'default'}
          editable={props.editable}
          secureTextEntry={props.passwordInput ? state.secureText : false}
          onChangeText={text =>
            updateState({
              searchText: text,
            })
          }
          placeholderTextColor={colors.gray11}
          style={{
            fontSize: wp(3.5),
            fontFamily: 'Poppins-Regular',
            color: colors.black,
            height: hp(15),
            textAlignVertical: 'top',
          }}
          placeholder={'Input Text ...'}
        />
      </Card>
      <TouchableOpacity onPress={() => props.onSearch(state.searchText)}>
        <Card
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.blue01,
            borderRadius: wp(0.7),
            height: hp(5),
          }}>
          <FontAweSome name="search" color={colors.white} size={hp(2.5)} />
          <Text
            style={{
              color: colors.white,
              marginLeft: wp(1),
              fontSize: wp(3.5),
              fontWeight: 'bold',
            }}>
            SEARCH
          </Text>
        </Card>
      </TouchableOpacity>
    </View>
  );
};

export default SearchMenu;
