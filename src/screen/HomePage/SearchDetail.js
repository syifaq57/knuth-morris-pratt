/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {Card} from 'native-base';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {useNavigation} from '@react-navigation/native';

import {kmpSearch} from '../Utils';

import database from '@react-native-firebase/database';

const initialState = {
  searchText: '',
  detailText: null,
  indexFound: null,
};

const SearchDetail = ({}, ref) => {
  const navigation = useNavigation();

  const [state, setState] = useState(initialState);
  const [kamusList, setKamusList] = useState([]);

  const updateState = newData => {
    setState(prev => ({
      ...prev,
      ...newData,
    }));
  };

  const setSearchText = val => {
    updateState({
      searchText: val,
    });
  };

  useImperativeHandle(ref, () => ({
    setSearchText,
  }));

  const fetchDetail = data => {
    const list = data;

    const result = kmpSearch(state.searchText, list);
    console.log('data', result);
    if (result) {
      updateState({
        detailText: list[result.indexData],
        indexFound: result,
      });
    } else {
      updateState(initialState);
    }
  };

  useEffect(() => {
    if (kamusList.length > 0) {
      fetchDetail(kamusList);
    }
  }, [state.searchText]);

  const fetchData = async () => {
    database()
      .ref('kamus')
      .orderByChild('title')
      .on('value', snapshot => {
        const kamuslist = [];
        snapshot.forEach(item => {
          kamuslist.push({
            id: item.key,
            title: item.val().title,
            desc: item.val().desc,
          });
        });
        setKamusList(kamuslist);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const isShowDetail = () => {
    if (state.searchText.length > 0 && state.detailText) {
      return true;
    }
    return false;
  };

  return (
    <Card
      style={{
        backgroundColor: colors.gray01,
        borderRadius: wp(0.5),
        paddingHorizontal: wp(1.8),
        paddingTop: hp(1),
        paddingBottom: hp(2),
        minHeight: hp(10),
      }}>
      {isShowDetail() ? (
        <DetailView
          detail={state.detailText || ''}
          onPressInfo={() => {
            navigation.navigate('DetailKMP', {detail: JSON.stringify(state)});
          }}
        />
      ) : (
        <BlankView />
      )}
    </Card>
  );
};

const BlankView = props => {
  return (
    <View style={{alignItems: 'center', flex: 1, justifyContent: 'center'}}>
      <Text style={{fontSize: wp(3)}}>
        {props.detailLabel ? props.detailLabel : 'Detail Tidak Ada'}
      </Text>
    </View>
  );
};

const DetailView = props => {
  return (
    <View style={{alignItems: 'flex-start'}}>
      <TouchableOpacity
        onPress={() => {
          props.onPressInfo();
        }}
        style={{
          marginLeft: wp(-0.5),
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          marginTop: hp(0.8),
        }}>
        <MaterialIcons name="info" color={colors.blue01} size={hp(3)} />
        <Text
          style={{
            fontSize: wp(3.5),
            color: colors.blue01,
            fontWeight: 'bold',
            marginLeft: wp(1),
          }}>
          Knuth Morris Pratt
        </Text>
      </TouchableOpacity>
      <View
        style={{marginTop: hp(1), marginHorizontal: wp(2), minHeight: hp(15)}}>
        <Text
          style={{
            fontSize: wp(2.9),
            fontWeight: 'bold',
          }}>{`${props.detail.title.toUpperCase()}`}</Text>
        <Text style={{fontSize: wp(3)}}>{props.detail.desc}</Text>
      </View>
    </View>
  );
};

export default forwardRef(SearchDetail);
