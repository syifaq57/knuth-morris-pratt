import React, {useRef} from 'react';
import {View, StyleSheet} from 'react-native';
import DeafaultHeader from '../../component/DeafaultHeader';
import SearchMenu from './SearchMenu';
import SearchDetail from './SearchDetail';
import colors from '../../res/colors';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import AppLabel from './AppLabel';

import ContainerScrollable from '../../component/ContainerScrollable';

const HomeScreen = () => {
  const searchDetail = useRef(null);

  const _onSearch = text => {
    searchDetail.current?.setSearchText(text);
  };

  return (
    <View style={styles.container}>
      <DeafaultHeader title="Dashboard" menuBar />
      <ContainerScrollable>
        <AppLabel />
        <SearchMenu
          onSearch={text => {
            _onSearch(text);
          }}
        />

        <SearchDetail ref={searchDetail} />
        <View style={{height: hp(15)}} />
      </ContainerScrollable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  body: {
    flex: 1,
    paddingHorizontal: wp(3),
    paddingVertical: hp(2),
  },
});

export default HomeScreen;
