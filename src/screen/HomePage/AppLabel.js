import React from 'react';
import {View, Text} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import colors from '../../res/colors';

const AppLabel = () => {
  return (
    <View style={{paddingVertical: hp(4), alignItems: 'center'}}>
      <Text style={{fontSize: wp(6), fontWeight: 'bold', color: colors.blue01}}>
        Kamus Kebidanan
      </Text>
      <Text>Aplikasi Pencarian Istilah dan Kamus Kebidanan</Text>
    </View>
  );
};

export default AppLabel;
