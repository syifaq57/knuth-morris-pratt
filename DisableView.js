import React from 'react';
import {View, Text} from 'react-native';

const DisableView = () => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 15, fontWeight: 'bold'}}>
        Trial period of the application has expired
      </Text>
      <Text style={{fontSize: 15, fontWeight: 'bold'}}>
        Please Contact Developer
      </Text>
    </View>
  );
};

export default DisableView;
